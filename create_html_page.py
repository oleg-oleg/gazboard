from jinja2 import Environment, FileSystemLoader
from glob import glob
import json
import xlrd

#    n 0  -  ['Потенциал']
#    n 1  -  ['Генерация гипотез', 'Проекция существующих техпроектов на бизнес-заказ', 'Скаутинг']
#    n 2  -  ['Потенциал ПО']
#    n 3  -  ['О - БНТ Команда оценки техпотенциала', 'СП - Эксперты функций', 'И – ДО']
#    n 4  -  ['']
#    n 5  -  ['ИДЕЯ']
#    n 6  -  ['']
#    n 7  -  ['Интерактивный опрос (menti.com)']
#    n 8  -  1


def root():

    class_list = {'ВЫЗОВЫ':[], 'ГИПОТЕЗЫ':[], 'РЕШЕНИЯ':[], 'ТИРАЖИРОВАНИЕ':[], 'КОММЕРЦИАЛИЗАЦИЯ':[]}

    excel_files = glob('./input/*')
    xl_workbook = xlrd.open_workbook(excel_files[0])
    sheet_names = xl_workbook.sheet_names()    
    
    for page_index in range(len(sheet_names)):

        xl_sheet = xl_workbook.sheet_by_index(page_index)        # xl_sheet = xl_workbook.sheet_by_name(sheet_names[0])

        for row_num in range(1, xl_sheet.nrows):          
            row = xl_sheet.row(row_num)                          # считываем строку

            box_content = []
            for i in range(1, len(row)):
                #content = row[i].value.split('\n')

                #if len(content) > 1 :
                #    box_content.append( content )   
                #else :
                #    box_content.append(row[i].value)
                box_content.append(row[i].value.split('\n'))   
            
            box_content.append(page_index + 1)

            class_list[(row[0].value).upper()].append(box_content)     # номер класса куда помещаем информацию

    #print(class_list)
    for i in class_list:
        for j in class_list[i]:
            for c in range(len(j)):
                print('n', c, ' - ', j[c])


    # return render_template("index.html", class_list=class_list)

    file_loader = FileSystemLoader('templates')
    env = Environment(loader=file_loader)
    env.trim_blocks = True
    env.lstrip_blocks = True
    env.rstrip_blocks = True

    template = env.get_template('index.html')

    output = template.render(class_list=class_list)
    
    f = open( 'created_index.html', 'w' )
    f.write(output)
    f.close()



if __name__ == "__main__":
    root()
